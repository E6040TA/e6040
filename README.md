# ECBM E6040 Neural Networks and Deep Learning
This repository contains the course materials of ECBM E6040,
"Neural Networks and Deep Learning," during Spring 2016 at
[Columbia University](http://www.columbia.edu/).

### Instructor
* Professor [Aurel A. Lazar](http://www.ee.columbia.edu/~aurel/)

## Homework Materials
* [Git Instruction](git_intro.md)
* [EC2 Introduction](http://goo.gl/ALIrHi)